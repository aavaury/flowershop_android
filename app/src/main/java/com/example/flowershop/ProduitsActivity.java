package com.example.flowershop;
//https://trinitytuts.com/capture-image-upload-server-android/
//https://uniqueandrocode.com/android-upload-image-to-server/

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.crashlytics.buildtools.reloc.org.apache.http.HttpResponse;
import com.google.firebase.crashlytics.buildtools.reloc.org.apache.http.NameValuePair;
import com.google.firebase.crashlytics.buildtools.reloc.org.apache.http.client.HttpClient;
import com.google.firebase.crashlytics.buildtools.reloc.org.apache.http.client.entity.UrlEncodedFormEntity;
import com.google.firebase.crashlytics.buildtools.reloc.org.apache.http.client.methods.HttpPost;
import com.google.firebase.crashlytics.buildtools.reloc.org.apache.http.impl.client.DefaultHttpClient;
import com.google.firebase.crashlytics.buildtools.reloc.org.apache.http.message.BasicNameValuePair;
import com.google.firebase.crashlytics.buildtools.reloc.org.apache.http.util.EntityUtils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


//Penser a installer les dépendance si ce n'est pas fait : clique droit sur l'erreur + install dependances ..
//Penser a décocher Typo In word
//ouvrir File > Settingset décocher Typol'intérieur Editor > Inspections > Proofreading

public class ProduitsActivity extends AppCompatActivity {
    String ms = "";
    EditText txtNomProduit, txtPrix;//Ajouter toutes les Editext présent dans l'activity xml
    Button btnInsererProduit;
    private ImageView img;
    private Button selectImg, uploadImg;
    private Uri filepath;
    Bitmap bitmap;
    final int IMAGE_REQUEST_CODE = 999;
    String connstr = "http://192.168.1.16/flowershop/sitePlante/insertion_produit.php";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_produits);

        txtPrix = (EditText) findViewById(R.id.txtPrix);// déclarer que la variable txtPrix coresspond a l'input id : txtPrix
        txtNomProduit = (EditText) findViewById(R.id.txtNomProduit);
        btnInsererProduit = (Button) findViewById(R.id.btnInsererProduit);

        img = (ImageView) findViewById(R.id.Imageprev);
        selectImg = (Button) findViewById(R.id.select_img);


        selectImg.setOnClickListener(new View.OnClickListener() {//quand le boutton choisir image dans la galery est cliqué
            @Override
            public void onClick(View v) {
                ActivityCompat.requestPermissions(ProduitsActivity.this,new String[] {Manifest.permission.READ_EXTERNAL_STORAGE},IMAGE_REQUEST_CODE);
            }
        });

        btnInsererProduit.setOnClickListener(new View.OnClickListener() { //bouton upload l'image
            @Override
            public void onClick(View v) {
                StringRequest stringRequest=new StringRequest(Request.Method.POST, connstr, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String,String> parms=new HashMap<String, String>();
                        String imgdata=imgToString(bitmap);
                        parms.put("imageurl",imgdata);
                        return parms;
                    }
                };
                String nom = txtNomProduit.getText().toString(); // récupère la valeurs dans l'input
                String prix = txtPrix.getText().toString(); // récupère la valeurs dans l'input
                String image = imgToString(bitmap);//recupere la valeur de l'image en base64
                bg_insertion_produit bg = new bg_insertion_produit(ProduitsActivity.this);
                bg.execute(nom, prix, image);

            }
        });
    } //fin onCreate

    // class qui insert les produits dans la base
    private class bg_insertion_produit extends AsyncTask<String, Void, String> {

        AlertDialog dialog;
        Context context;

        public bg_insertion_produit(Context context) {//constructeur
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new AlertDialog.Builder(context).create();
            dialog.setTitle("Etat de connexion");
        }

        @Override
        protected String doInBackground(String... strings) {

            String result = "";

            String strNom = strings[0];// déclaration de la variable (ici c'est le nom du produit)
            String prixproduit = strings[1]; //déclaration de la varaibele (ici c'est le prix du produit)
            String imageProduit = strings[2]; //déclaration de la varaibele (ici c'est l'image en format base64)


            String connstr = "http://192.168.1.76/flowershop/sitePlante/insertion_produit.php";
            try {
                URL url = new URL(connstr);
                HttpURLConnection http = (HttpURLConnection) url.openConnection();
                http.setRequestMethod("POST");
                http.setDoInput(true);
                http.setDoOutput(true);
                OutputStream ops = http.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(ops, "UTF-8"));
                //data est la variable des données a insérrer
                String data = URLEncoder.encode("nom", "UTF-8") + "=" + URLEncoder.encode(strNom, "UTF-8") +
                        "&&" + URLEncoder.encode("prix", "UTF-8") + "=" + URLEncoder.encode(prixproduit, "UTF-8") +
                        "&&" + URLEncoder.encode("image", "UTF-8") + "=" + URLEncoder.encode(imageProduit, "UTF-8");

                writer.write(data);
                Log.v("ProduitsActivity", data);
                writer.flush();
                writer.close();
                InputStream ips = http.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(ips, "UTF-8"));
                String ligne = "";
                while ((ligne = reader.readLine()) != null) {
                    result += ligne;
                }
                reader.close();
                ips.close();
                http.disconnect();

                return result;


            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String s) { //execute l'insertion et affiche success si tout c'est bien passé
            super.onPostExecute(s);//execute

            dialog.setMessage(s);
            dialog.show();
            if (s.contains("succes insertion")) {
                Toast.makeText(context, "Produit inséré avec succès.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(context, "Problème d'insertion.", Toast.LENGTH_LONG).show();
            }
        }
    }


    // upload image

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode==IMAGE_REQUEST_CODE){
            if (grantResults.length>0&& grantResults[0]== PackageManager.PERMISSION_GRANTED){
                Intent intent=new Intent(new Intent(Intent.ACTION_PICK));
                intent.setType("image/*");

                startActivityForResult(Intent.createChooser(intent,"select image"),IMAGE_REQUEST_CODE);

            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode==IMAGE_REQUEST_CODE && resultCode==RESULT_OK && data!=null){
            filepath=data.getData();
            try {
                InputStream inputStream=getContentResolver().openInputStream(filepath);
                bitmap= BitmapFactory.decodeStream(inputStream);
                img.setImageBitmap(bitmap);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
    private String imgToString(Bitmap bitmap){ //convertit l'image en base
        ByteArrayOutputStream byteArrayOutputStream=new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG,100,byteArrayOutputStream);
        byte[] imgbytes=byteArrayOutputStream.toByteArray();
        String encodeimg= Base64.encodeToString(imgbytes,Base64.DEFAULT);
        return encodeimg;
    }

    @Override /* importe le menu */
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu2, menu);
        return true;
    }

    @Override /* action des boutton du menu avec id */
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.login: /* boutton id login */
                /* Toast.makeText(getApplicationContext(), "Ouverture de la fenêtre de login !", Toast.LENGTH_LONG).show(); */
                Intent intent = new Intent(ProduitsActivity.this, MainActivity.class); /* Je suis sur la page new_client et éxécute le code de la class new_client */
                startActivity(intent);
                return true;
            case R.id.boutique: /* boutton id boutique */
                /* Toast.makeText(getApplicationContext(), "Ouverture de la fenêtre de produit !", Toast.LENGTH_LONG).show(); */
                Intent intent2 = new Intent(ProduitsActivity.this, Produit.class); /* Je suis sur la page new_client et éxécute le code de la class new_client */
                startActivity(intent2);
                return true;
            case R.id.quitter: /* boutton quitter (quitte l'application) */
                System.exit(0);
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}


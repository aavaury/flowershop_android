package com.example.flowershop;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class Produit extends AppCompatActivity {

    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.produits_boutique);

        listView = (ListView) findViewById(R.id.listView);
        getJSON("http://192.168.1.76/flowershop/sitePlante/getData.php");
    }


    private void getJSON(final String urlWebService) {

        class GetJSON extends AsyncTask<Void, Void, String> {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }


            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                try {
                    loadIntoListView(s);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            protected String doInBackground(Void... voids) {
                try {
                    URL url = new URL(urlWebService);
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    StringBuilder sb = new StringBuilder();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));
                    String json;
                    while ((json = bufferedReader.readLine()) != null) {
                        sb.append(json + "\n");
                    }
                    return sb.toString().trim();
                } catch (Exception e) {
                    return null;
                }
            }
        }
        GetJSON getJSON = new GetJSON();
        getJSON.execute();
    }

    private void loadIntoListView(String json) throws JSONException {
        JSONArray jsonArray = new JSONArray(json);
        String[] heroes = new String[jsonArray.length()];
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject obj = jsonArray.getJSONObject(i);
            heroes[i] = obj.getString("nom");
        }
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, heroes);
        listView.setAdapter(arrayAdapter);
    }

    @Override /* importe le menu */
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu3, menu);
        return true;
    }

    @Override /* action des boutton du menu avec id */
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.login: /* boutton id login */
                /* Toast.makeText(getApplicationContext(), "Ouverture de la fenêtre de login !", Toast.LENGTH_LONG).show(); */
                Intent intent = new Intent(Produit.this, MainActivity.class); /* Je suis sur la page new_client et éxécute le code de la class new_client */
                startActivity(intent);
                return true;
            case R.id.addProduit: /* boutton id addProduit */
                /* Toast.makeText(getApplicationContext(), "Ouverture de la fenêtre de ProduiActivity !", Toast.LENGTH_LONG).show(); */
                Intent intent2 = new Intent(Produit.this, ProduitsActivity.class); /* Je suis sur la page Produit et éxécute le code de la class ProduitActivity */
                startActivity(intent2);
                return true;
            case R.id.quitter: /* boutton quitter (quitte l'application) */
                System.exit(0);
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}